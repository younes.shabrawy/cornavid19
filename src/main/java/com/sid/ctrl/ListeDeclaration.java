package com.sid.ctrl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sid.dao.PersonneRepo;
import com.sid.entities.Personne;

@RestController
@CrossOrigin("*")
public class ListeDeclaration {
	@Autowired
private PersonneRepo personneRepo;
	@PostMapping( value="/personne")
	public Personne savePersonne(@RequestBody Personne personne) {
		return personneRepo.save(personne);
	}
	
	@GetMapping(value="/personne")
	public List<Personne> getPersonnes(){
	return	personneRepo.findAll();
	}
	
}
