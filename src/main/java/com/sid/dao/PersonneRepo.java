package com.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.sid.entities.Personne;
@CrossOrigin("*")
//@RepositoryRestResource(collectionResourceRel = "personne",path = "personne")
public interface PersonneRepo extends JpaRepository<Personne, Long>{

}
