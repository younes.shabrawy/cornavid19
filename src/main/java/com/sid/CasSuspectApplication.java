package com.sid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sid.dao.PersonneRepo;
import com.sid.entities.Personne;

@SpringBootApplication
public class CasSuspectApplication implements CommandLineRunner{
@Autowired
	private PersonneRepo personneRepo;
	public static void main(String[] args)  {
		SpringApplication.run(CasSuspectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		personneRepo.save(new Personne("younes", "chabraoui", "0665275473", "hay farah", "Bonjour, je veux déclarer d'une cas dans notre ville !"));
		personneRepo.save(new Personne("btyssam", "ouarchane", "0665275472", "belveder", "salam je veux declarer dans notre quartier !"));
		personneRepo.save(new Personne("ayoub", "anbara", "0665275471", "eljadida", "salam je veux declarer dans notre region !"));
	}

}
